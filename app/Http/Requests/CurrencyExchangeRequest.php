<?php

namespace App\Http\Requests;

use App\Enums\CurrencyCode;
use App\Rules\ValidAmount;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class CurrencyExchangeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'src' => ['required', new EnumValue(CurrencyCode::class)],
            'target' => ['required', new EnumValue(CurrencyCode::class)],
            'amount' => ['required', new ValidAmount()],
        ];
    }

    /**
     * @throws HttpResponseException
     */
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'message' => 'fail',
            'errors' => $validator->errors(),
        ], 422));
    }
}
