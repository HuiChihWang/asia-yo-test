<?php

namespace App\Http\Controllers;

use App\Enums\CurrencyCode;
use App\Http\Requests\CurrencyExchangeRequest;
use App\Services\CurrencyExchangeService;

class CurrencyController extends Controller
{
    protected CurrencyExchangeService $currencyExchangeService;

    public function __construct(CurrencyExchangeService $currencyExchangeService)
    {
        $this->currencyExchangeService = $currencyExchangeService;
    }

    /**
     * @OA\Get(
     *     path="/currency/exchange",
     *     tags={"Currency"},
     *     summary="Exchange currency with amount (with precision 2)",
     *     @OA\Parameter  (
     *         name="src",
     *         in="query",
     *         description="source currency code",
     *         required=true,
     *         @OA\Schema(type="string", enum={"TWD", "USD", "JPY"}),
     *     ),
     *     @OA\Parameter  (
     *          name="target",
     *          in="query",
     *          description="target currency code",
     *          required=true,
     *          @OA\Schema(type="string", enum={"TWD", "USD", "JPY"}),
     *     ),
     *     @OA\Parameter  (
     *          name="amount",
     *          in="query",
     *          description="amount",
     *          required=true,
     *          @OA\Schema(type="string"),
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="success",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="amount", type="string", example="1,000.62"),
     *          )
     *     )
     * )
     */
    public function exchangeCurrency(CurrencyExchangeRequest $request)
    {
        $src = CurrencyCode::fromValue($request->input('src'));
        $target = CurrencyCode::fromValue($request->input('target'));
        $amount = $request->input('amount');

        $result = $this->currencyExchangeService->exchange($src, $target, $amount);

        return response()->json([
            'message' => 'success',
            'amount' => $result,
        ]);
    }
}
