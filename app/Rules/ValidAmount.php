<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class ValidAmount implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $amountPattern = '/^\d+(,\d{3})*(\.\d+)?$/';
        if (!preg_match($amountPattern, $value)) {
            $fail("The $attribute must be in a valid price format (e.g., \"1000\" or \"1,000\").");
        }
    }
}
