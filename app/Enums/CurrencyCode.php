<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

final class CurrencyCode extends Enum
{
    const TWD = 'TWD';
    const USD = 'USD';
    const JPY = 'JPY';
}
