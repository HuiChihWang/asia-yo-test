<?php

namespace App\Services;


use App\Enums\CurrencyCode;
use InvalidArgumentException;

class CurrencyExchangeService {

    protected $currencyExchangeRate;

    public function __construct($currencyExchangeRate)
    {
        $this->currencyExchangeRate = $currencyExchangeRate;
    }

    public function exchange(string $src, string $target, string $amount): string
    {
        if (!CurrencyCode::hasValue($src) || !CurrencyCode::hasValue($target)) {
            throw new InvalidArgumentException('currency code is invalid');
        }

        $amount = $this->convertAmountStringToAmountFloat($amount);

        if ($amount < 0) {
            throw new InvalidArgumentException('amount is invalid');
        }
        $rate = $this->getRate($src, $target);
        $roundedAmount = round($amount, 2);
        return number_format($roundedAmount * $rate, 2, '.', ',');
    }

    private function convertAmountStringToAmountFloat(string $amount): float
    {
        return (float) str_replace(',', '', $amount);
    }

    private function getRate(string $src, string $target): float
    {
        return $src === $target ? 1.0 : $this->currencyExchangeRate[$src][$target];
    }
}
