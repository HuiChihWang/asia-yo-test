<?php

namespace App\Providers;

use App\Enums\CurrencyCode;
use App\Services\CurrencyExchangeService;
use Illuminate\Support\ServiceProvider;
use WeakMap;

class CurrencyExchangeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->singleton(CurrencyExchangeService::class, function ($app) {
            return new CurrencyExchangeService([
                CurrencyCode::TWD => [
                    CurrencyCode::USD => 0.03281,
                    CurrencyCode::JPY => 3.669,
                ],
                CurrencyCode::USD => [
                    CurrencyCode::TWD => 30.444,
                    CurrencyCode::JPY => 111.801,
                ],
                CurrencyCode::JPY => [
                    CurrencyCode::TWD => 0.26956,
                    CurrencyCode::USD => 0.00885,
                ],
            ]);
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
