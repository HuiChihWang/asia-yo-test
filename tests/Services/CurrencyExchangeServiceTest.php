<?php

namespace Tests\Services;

use App\Enums\CurrencyCode;
use App\Services\CurrencyExchangeService;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class CurrencyExchangeServiceTest extends TestCase
{
    private static function createTestService(): CurrencyExchangeService
    {
        return new CurrencyExchangeService([
            CurrencyCode::TWD => [
                CurrencyCode::USD => 0.03281,
                CurrencyCode::JPY => 3.669,
            ],
            CurrencyCode::USD => [
                CurrencyCode::TWD => 30.444,
                CurrencyCode::JPY => 111.801,
            ],
            CurrencyCode::JPY => [
                CurrencyCode::TWD => 0.26956,
                CurrencyCode::USD => 0.00885,
            ],
        ]);
    }

    public static function dataProviderForTestExchange()
    {
        return [
            ['TWD', 'USD', '100', '3.28'],
            ['TWD', 'JPY', '100', '366.90'],
            ['USD', 'TWD', '100', '3,044.40'],
            ['USD', 'JPY', '100.3567', '11,220.35'],
            ['JPY', 'TWD', '99.9999', '26.96'],
            ['JPY', 'USD', '33.098', '0.29'],
            ['TWD', 'TWD', '1,000', '1,000.00'],
            ['USD', 'USD', '1,2345', '12,345.00'],
            ['JPY', 'JPY', '1,666.789', '1,666.79']
        ];
    }

    /**
     * @dataProvider dataProviderForTestExchange
     */
    public function testExchange($srcCurrency, $targetCurrency, $amount, $expected)
    {
        $currencyExchangeService = self::createTestService();
        $this->assertEquals($expected, $currencyExchangeService->exchange($srcCurrency, $targetCurrency, $amount));
    }

    public static function dataProviderForTestExchangeWhenInputIsInvalid()
    {
        return [
            ['TWD', 'INVALID', 100],
            ['INVALID', 'USD', 100],
            ['TWD', 'USD', -10.444],
            ['TWD', 'USD', -100],
            ['TWD', 'UUU', '1,56,98.hhh'],
        ];
    }

    /**
     * @dataProvider dataProviderForTestExchangeWhenInputIsInvalid
     */
    public function testExchangeWhenInputIsInvalid($srcCurrency, $targetCurrency, $amount)
    {
        $currencyExchangeService = self::createTestService();
        $this->expectException(InvalidArgumentException::class);
        $currencyExchangeService->exchange($srcCurrency, $targetCurrency, $amount);
    }
}
