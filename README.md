## Asia-Yo Test

### SQL Test

1. Query the top 10 most expensive orders (bnb_id, bnb_name, may_amount) in 2023 May with currencyCode TWD and

```sql
SELECT orders.bnb_id AS bnb_id,
       bnbs.name     AS bnb_name,
       SUM(amount)   AS may_amount
FROM orders
         JOIN bnbs
              ON orders.bnb_id = bnbs.id
WHERE 
    orders.created_at BETWEEN '2023-05-01 00:00:00' AND '2023-05-31 23:59:59'
    AND orders.currency = 'TWD'
GROUP BY orders.bnb_id, bnbs.name
ORDER BY may_amount DESC LIMIT 10;
```

2. If the query is very slow, we can use `EXPLAIN` keyword to check the query plan and find the bottleneck.
As we see in table query, we can join table later after filtering the data because actually we do not need to use bnbs table to aggregate total amount.

```sql
SELECT bnbs.id AS bnb_id,
       bnbs.name AS bnb_name,
       amount_table.may_amount
FROM (
        SELECT
            bnb_id,
            SUM(amount) AS may_amount
        FROM orders
        WHERE 
            orders.created_at BETWEEN '2023-05-01 00:00:00' AND '2023-05-31 23:59:59'
            AND orders.currency = 'TWD'
        ORDER BY may_amount DESC 
        LIMIT 10;
    ) AS amount_table
 JOIN bnbs
      ON amount_table.bnb_id = bnbs.id
```

Besides, we can add index `currency` column to speed up the query if we need to filter result based on different currency code often.

### API Test

Run app

```bash
./vender/bin/sail up
```

Swagger
 - http://localhost:3001/api/documentation

### Experience Sharing
https://docs.google.com/document/d/1GkWhKnrzrwCmK_vycnRvkDjsw8AIYp4DjteLoeX9G8A/edit?usp=sharing
